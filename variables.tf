variable "wait_for_resources" {}
variable "NAMESPACE" {
  default = "cicd"
}
variable "gitlab_server" {
  default = "https://gitlab.com/"
  description = "Gitlab server"
}
variable "GITLAB_GROUP" {
  description = "Gitlab group"
}
variable "gitlab_runner_token" {
  description = "Gitlab Runner registration token"
}