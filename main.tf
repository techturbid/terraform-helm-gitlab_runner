resource "null_resource" "wait_for_resources" {
  triggers = {
    depends_on = join("", var.wait_for_resources)
  }
}

data "helm_repository" "gitlab" {
  depends_on = ["null_resource.wait_for_resources"]
  name = "gitlab"
  url  = "https://charts.gitlab.io"
}

resource "helm_release" "gitlab-runner" {
  depends_on = [data.helm_repository.gitlab.metadata[0].name]
  chart = "gitlab-runner"
  name = "${var.GITLAB_GROUP}-gitlab-runner"
  repository = data.helm_repository.gitlab.metadata[0].name
  namespace = var.NAMESPACE

  set {
    name = "gitlabUrl"
    value = var.gitlab_server
  }

  set {
    name = "runnerRegistrationToken"
    value = var.gitlab_runner_token
  }

  set {
    name = "rbac.create"
    value = "true"
  }
}
